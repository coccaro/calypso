/*
  Copyright (C) 2020 CERN for the benefit of the FASER collaboration
*/

// Local include(s):
#include "xAODFaserCalorimeter/versions/CalorimeterHitAuxContainer_v1.h"

namespace xAOD {

  CalorimeterHitAuxContainer_v1::CalorimeterHitAuxContainer_v1() 
    : AuxContainerBase() {

    AUX_VARIABLE(localtime);
    AUX_VARIABLE(bcid_time);
    AUX_VARIABLE(raw_energy);

    AUX_VARIABLE(caloLinks);
  }

} // namespace xAOD

