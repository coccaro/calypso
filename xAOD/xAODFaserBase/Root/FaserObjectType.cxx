// System include(s):
#include <iostream>

// Local include(s):
#include "xAODFaserBase/FaserObjectType.h"

/// Helper macro for printing the object type as a string
#define PRINT_TYPE( TYPE )                      \
   case TYPE:                                   \
      out << #TYPE;                             \
      break

/// This function can be used in (debug) printouts to easily show the type
/// name returned by an object.
///
/// @param out The STL stream to print to
/// @param type The type whose name to print in the stream
/// @returns The same stream that it received
///
std::ostream& operator<< ( std::ostream& out, xAOD::FaserType::ObjectType type ) {

   switch( type ) {

      PRINT_TYPE( xAOD::FaserType::Other );

      PRINT_TYPE( xAOD::FaserType::CaloCluster );
      PRINT_TYPE( xAOD::FaserType::Track );
      PRINT_TYPE( xAOD::FaserType::NeutralParticle );
      PRINT_TYPE( xAOD::FaserType::Electron );
      PRINT_TYPE( xAOD::FaserType::Photon );
      PRINT_TYPE( xAOD::FaserType::Muon );

      PRINT_TYPE( xAOD::FaserType::Vertex );

      PRINT_TYPE( xAOD::FaserType::FaserTruthParticle );
      PRINT_TYPE( xAOD::FaserType::FaserTruthVertex );
      PRINT_TYPE( xAOD::FaserType::FaserTruthEvent );
      PRINT_TYPE( xAOD::FaserType::FaserTruthPileupEvent );
      
      PRINT_TYPE( xAOD::FaserType::EventInfo );
      PRINT_TYPE( xAOD::FaserType::EventFormat );

      PRINT_TYPE( xAOD::FaserType::Particle );
      PRINT_TYPE( xAOD::FaserType::CompositeParticle );

   default:
      out << "UNKNOWN";
      break;
   }

   // Return the stream object:
   return out;
}
