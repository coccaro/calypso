# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

import sys
from AthenaCommon.Logging import log, logging
from AthenaCommon.Constants import DEBUG, VERBOSE, INFO
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
# from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
# from Digitization.DigitizationParametersConfig import writeDigitizationMetadata
from TrackerPrepRawDataFormation.TrackerPrepRawDataFormationConfig import FaserSCT_ClusterizationCfg
from TrackerSpacePointFormation.TrackerSpacePointFormationConfig import TrackerSpacePointFinderCfg
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from FaserSCT_GeoModel.FaserSCT_GeoModelConfig import FaserSCT_GeometryCfg

Tracker__TruthSeededTrackFinder, THistSvc=CompFactory.getComps("Tracker::FaserSpacePoints", "THistSvc")


def TruthSeededTrackFinderBasicCfg(flags, **kwargs):
    """Return ComponentAccumulator for TruthSeededTrackFinder"""
    acc = FaserSCT_GeometryCfg(flags)
    kwargs.setdefault("SpacePointsSCTName", "SCT_SpacePointContainer")
    acc.addEventAlgo(Tracker__TruthSeededTrackFinder(**kwargs))
   # attach ToolHandles
    return acc

def TruthSeededTrackFinder_OutputCfg(flags):
    """Return ComponentAccumulator with Output for SCT. Not standalone."""
    acc = ComponentAccumulator()
    acc.merge(OutputStreamCfg(flags, "ESD"))
    ostream = acc.getEventAlgo("OutputStreamESD")
    ostream.TakeItemsFromInput = True
    return acc

def FaserSpacePointsCfg(flags, **kwargs):
    acc=TruthSeededTrackFinderBasicCfg(flags, **kwargs)
    histSvc= THistSvc()
    histSvc.Output += [ "TruthTrackSeeds DATAFILE='truthtrackseeds.root' OPT='RECREATE'" ]
    acc.addService(histSvc)
    acc.merge(TruthSeededTrackFinder_OutputCfg(flags))
    return acc

if __name__ == "__main__":
  log.setLevel(DEBUG)
  Configurable.configurableRun3Behavior = True

  # Configure
  ConfigFlags.Input.Files = ['my.RDO.pool.root']
  ConfigFlags.Output.ESDFileName = "mySeeds.ESD.pool.root"
  ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"             # Always needed; must match FaserVersion
  ConfigFlags.IOVDb.DatabaseInstance = "OFLP200"               # Use MC conditions for now
  ConfigFlags.Input.ProjectName = "data20"                     # Needed to bypass autoconfig
  ConfigFlags.Input.isMC = False                               # Needed to bypass autoconfig
  ConfigFlags.GeoModel.FaserVersion     = "FASER-01"           # FASER cosmic ray geometry (station 2 only)
  ConfigFlags.Common.isOnline = False
  ConfigFlags.GeoModel.Align.Dynamic = False
  ConfigFlags.Beam.NumberOfCollisions = 0.

  ConfigFlags.lock()

  # Core components
  acc = MainServicesCfg(ConfigFlags)
  acc.merge(PoolReadCfg(ConfigFlags))

  #acc.merge(writeDigitizationMetadata(ConfigFlags))

  # Inner Detector
  acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags))
  acc.merge(TrackerSpacePointFinderCfg(ConfigFlags))
  acc.merge(FaserSpacePointsCfg(ConfigFlags))

  # Timing
  #acc.merge(MergeRecoTimingObjCfg(ConfigFlags))

  # Dump config
  logging.getLogger('forcomps').setLevel(VERBOSE)
  acc.foreach_component("*").OutputLevel = VERBOSE
  acc.foreach_component("*ClassID*").OutputLevel = INFO
  # acc.getCondAlgo("FaserSCT_AlignCondAlg").OutputLevel = VERBOSE
  # acc.getCondAlgo("FaserSCT_DetectorElementCondAlg").OutputLevel = VERBOSE
  acc.getService("StoreGateSvc").Dump = True
  acc.getService("ConditionStore").Dump = True
  acc.printConfig(withDetails=True)
  ConfigFlags.dump()

  # Execute and finish
  sc = acc.run(maxEvents=-1)

  # Success should be 0
  sys.exit(not sc.isSuccess())
