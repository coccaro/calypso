#include "TrackerSpacePoint/TrackerSeed.h"

namespace Tracker {

  TrackerSeed::TrackerSeed() : m_strategyId(NULLID) {}

  TrackerSeed::TrackerSeed(const StrategyId id, const TrackerSeed& trackerSeed) : m_strategyId(id), m_seed(trackerSeed.m_seed) {}

  TrackerSeed::~TrackerSeed() {}

  TrackerSeed::TrackerSeed(const StrategyId id, vector<const FaserSCT_SpacePoint*> seed) { m_strategyId = id; m_seed = seed; }
  
  void TrackerSeed::add(vector<const FaserSCT_SpacePoint*> seed) { m_seed = seed; }
  
  int TrackerSeed::size() const { return m_seed.size(); }

  TrackerSeed& TrackerSeed::operator=(const TrackerSeed& trackSeed){
    if(&trackSeed != this) {
      TrackerSeed::operator=(trackSeed);
      m_seed = trackSeed.m_seed;
    }
    return *this;
  }

  MsgStream& TrackerSeed::dump(MsgStream& stream) const {
    stream << "TrackerSeed object" << endl;
    this->TrackerSeed::dump(stream);
    return stream;
  }

  ostream& TrackerSeed::dump(ostream& stream) const {
    stream << "TrackerSeed object" << endl;
    this->TrackerSeed::dump(stream);
    return stream;
  }

  MsgStream& operator << (MsgStream& stream, const TrackerSeed& trackSeed) {
    return trackSeed.dump(stream);
  }

  ostream& operator << (ostream& stream, const TrackerSeed& trackSeed) {
    return trackSeed.dump(stream);
  }
}
