#ifndef CALODIGIALGS_CALOWAVEFORMDIGIALG_H
#define CALODIGIALGS_CALOWAVEFORMDIGIALG_H

// Base class
#include "AthenaBaseComps/AthReentrantAlgorithm.h"

// Data classes
#include "WaveRawEvent/RawWaveformContainer.h"
#include "FaserCaloSimEvent/CaloHitCollection.h"

// Tool classes
#include "WaveDigiTools/IWaveformDigitisationTool.h"

// Handles
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"

// Gaudi
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"

// ROOT
#include "TF1.h"

// STL
#include <string>

class CaloWaveformDigiAlg : public AthReentrantAlgorithm {

 public:
  // Constructor
  CaloWaveformDigiAlg(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~CaloWaveformDigiAlg() = default;

  /** @name Usual algorithm methods */
  //@{
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext& ctx) const override;
  virtual StatusCode finalize() override;
  //@}

 private:

  /** @name Disallow default instantiation, copy, assignment */
  //@{
  CaloWaveformDigiAlg() = delete;
  CaloWaveformDigiAlg(const CaloWaveformDigiAlg&) = delete;
  CaloWaveformDigiAlg &operator=(const CaloWaveformDigiAlg&) = delete;
  //@}

  Gaudi::Property<double> m_CB_alpha {this, "CB_alpha", 0, "Alpha of the crystal ball function"};
  Gaudi::Property<double> m_CB_n {this, "CB_n", 0, "n of the crystal ball function"};
  Gaudi::Property<double> m_CB_mean {this, "CB_mean", 0, "Mean of the crystal ball function"};
  Gaudi::Property<double> m_CB_sigma {this, "CB_sigma", 0, "Sigma of the crystal ball function"};
  Gaudi::Property<double> m_CB_norm {this, "CB_norm", 0, "Norm of the crystal ball function"};

  Gaudi::Property<double> m_base_mean {this, "base_mean", 0, "Mean of the baseline"};
  Gaudi::Property<double> m_base_rms {this, "base_rms", 0, "RMS of the baseline"};

  /// Kernel PDF
  TF1* m_kernel;


  /**
   * @name Digitisation tool
   */
  ToolHandle<IWaveformDigitisationTool> m_digiTool
    {this, "WaveformDigitisationTool", "WaveformDigitisationTool"};


  /**
   * @name Input HITS using SG::ReadHandleKey
   */
  //@{

  SG::ReadHandleKey<CaloHitCollection> m_caloHitContainerKey 
  {this, "CaloHitContainerKey", ""};

  //@}


  /**
   * @name Output data using SG::WriteHandleKey
   */
  //@{
  SG::WriteHandleKey<RawWaveformContainer> m_waveformContainerKey
    {this, "WaveformContainerKey", ""};
  //@}

};

#endif // CALODIGIALGS_CALODIGIALG_H
