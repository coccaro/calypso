""" Define methods used to instantiate configured Waveform reconstruction tools and algorithms

Copyright (C) 2020 CERN for the benefit of the FASER collaboration
"""
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg

#CalorimeterReconstructionTool = CompFactory.CalorimeterReconstructionTool

# One stop shopping for normal FASER data
def CalorimeterReconstructionCfg(flags, **kwargs):
    """ Return all algorithms and tools for Waveform reconstruction """
    acc = ComponentAccumulator()

    #tool = CalorimeterReconstructionTool(name="CaloRecTool", **kwargs)

    kwargs.setdefault("CaloWaveHitContainerKey", "CaloWaveformHits")
    kwargs.setdefault("PreshowerWaveHitContainerKey", "PreshowerWaveformHits")
    kwargs.setdefault("CaloHitContainerKey", "CaloHits")
    #kwargs.setdefault("CalorimeterReconstructionTool", tool)

    recoAlg = CompFactory.CaloRecAlg("CaloRecAlg", **kwargs)
    #recoAlg.CalorimeterReconstructionTool = tool
    acc.addEventAlgo(recoAlg)

    return acc

def CalorimeterReconstructionOutputCfg(flags, **kwargs):
    """ Return ComponentAccumulator with output for Calorimeter Reco"""
    acc = ComponentAccumulator()
    ItemList = [
        "xAOD::CalorimeterHitContainer#*"
        , "xAOD::CalorimeterHitAuxContainer#*"
    ]
    acc.merge(OutputStreamCfg(flags, "xAOD", ItemList))
    # ostream = acc.getEventAlgo("OutputStreamRDO")
    # ostream.TakeItemsFromInput = True # Don't know what this does
    return acc

