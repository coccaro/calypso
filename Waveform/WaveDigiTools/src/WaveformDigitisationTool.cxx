/*
  Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

/**
 * @file WaveformDigitisationTool.cxx
 * Implementation file for the WaveformDigitisationTool class
 * @ author C. Gwilliam, 2021
 **/

#include "WaveformDigitisationTool.h"

// Constructor
WaveformDigitisationTool::WaveformDigitisationTool(const std::string& type, const std::string& name, const IInterface* parent) :
  base_class(type, name, parent)
{
}

// Initialization
StatusCode
WaveformDigitisationTool::initialize() {
  ATH_MSG_INFO( name() << "::initalize()" );
  m_random = new TRandom3();
  return StatusCode::SUCCESS;
}


