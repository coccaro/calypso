/*
   Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

/** @file WaveformDigitisationTool.h
 *  Header file for WaveformDigitisationTool.h
 *
 */
#ifndef WAVEDIGITOOLS_WAVEFORMDIGITISATIONTOOL_H
#define WAVEDIGITOOLS_WAVEFORMDIGITISATIONTOOL_H

//Athena
#include "AthenaBaseComps/AthAlgTool.h"
#include "WaveDigiTools/IWaveformDigitisationTool.h"

//Gaudi
#include "GaudiKernel/ToolHandle.h"

//STL

class WaveformDigitisationTool: public extends<AthAlgTool, IWaveformDigitisationTool> {
 public:

  /// Normal constructor for an AlgTool; 'properties' are also declared here
 WaveformDigitisationTool(const std::string& type, 
			  const std::string& name, const IInterface* parent);

  /// Retrieve the necessary services in initialize
  StatusCode initialize();

 private:
  // None

};

#endif // WAVEDIGITOOLS_WAVEFORMDIGITISATIONTOOL_H
