/*
  Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

/**
 * @file IWaveformDigitisationTool.h
 * Header file for the IWaveformDigitisationTool class
 * @author Carl Gwilliam, 2021
 */


#ifndef WAVEDIGITOOLS_IWAVEFORMDIGITISATIONTOOL_H
#define WAVEDIGITOOLS_IWAVEFORMDIGITISATIONTOOL_H

// Base class
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/ToolHandle.h"

#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/MsgStream.h"

#include "WaveRawEvent/RawWaveformContainer.h"
#include "WaveRawEvent/RawWaveform.h"

#include "TF1.h"
#include "TRandom3.h"

#include <utility>

///Interface for waveform digitisation tools
class IWaveformDigitisationTool : virtual public IAlgTool 
{
public:

  // InterfaceID
  DeclareInterfaceID(IWaveformDigitisationTool, 1, 0);

  IWaveformDigitisationTool():
    m_msgSvc         ( "MessageSvc",   "ITrkEventCnvTool" )
  {}

  virtual ~IWaveformDigitisationTool() = default;

  // Digitise HITS to Raw waveform
  template<class CONT>
  StatusCode digitise(const CONT* hitCollection, 
		      RawWaveformContainer* waveContainer, 
		      TF1* kernel, std::pair<float, float> base
		      ) const;

private:
  ServiceHandle<IMessageSvc>      m_msgSvc;

protected:
  TRandom3*                       m_random;

};

#include "WaveDigiTools/IWaveformDigitisationTool.icc"


#endif //WAVEDIGITOOLS_IWAVEFORMDIGITISATIONTOOL_H
