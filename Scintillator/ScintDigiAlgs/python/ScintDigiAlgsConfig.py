# Copyright (C) 2020-2021 CERN for the benefit of the FASER collaboration

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg

# Crystallball parameter dictionary used in simulated digitized wave reconstruction.
# Crystalball function Parameters estimated from Deion's slides uploaded at
# https://indico.cern.ch/event/1099652/contributions/4626975/attachments/2352595/4013927/Faser-Physics-run3933-plots.pdf  (20/01/2022)
# Parameters are per scintillator source, but not per channel.
dict_CB_param = {}
dict_CB_param["Trigger"]=dict(CB_alpha=-0.38, CB_n=25, CB_mean=815, CB_sigma=7.7, CB_norm = 500 )
dict_CB_param["Timing"]=dict(CB_alpha=-0.32, CB_n=65, CB_mean=846, CB_sigma=5.3, CB_norm = 500) # copy from Preshower; Timing was not in TestBeam
dict_CB_param["Veto"]=dict(CB_alpha=-0.38, CB_n=25, CB_mean=815, CB_sigma=7.7, CB_norm = 1000) # copy from Trigger; Veto was not in TestBeam, but in sim "Veto" is the TestBeam Trigger component
dict_CB_param["Preshower"]=dict(CB_alpha=-0.32, CB_n=65, CB_mean=846, CB_sigma=5.3, CB_norm = 500)

# One stop shopping for normal FASER data
def ScintWaveformDigitizationCfg(flags):
    """ Return all algorithms and tools for Waveform digitization """
    acc = ComponentAccumulator()

    if not flags.Input.isMC:
        return acc

    if "TB" not in flags.GeoModel.FaserVersion:
        acc.merge(ScintWaveformDigiCfg(flags, "TimingWaveformDigiAlg", "Trigger"))
    acc.merge(ScintWaveformDigiCfg(flags, "VetoWaveformDigiAlg", "Veto"))
    acc.merge(ScintWaveformDigiCfg(flags, "PreshowerWaveformDigiAlg", "Preshower"))
    acc.merge(ScintWaveformDigitizationOutputCfg(flags))
    return acc

# Return configured digitization algorithm from SIM hits
# Specify data source (Veto, Trigger, Preshower)
def ScintWaveformDigiCfg(flags, name="ScintWaveformDigiAlg", source="", **kwargs):

    acc = ComponentAccumulator()

    tool = CompFactory.WaveformDigitisationTool(name=source+"WaveformDigtisationTool", **kwargs)
    
    kwargs.setdefault("ScintHitContainerKey", source+"Hits")
    kwargs.setdefault("WaveformContainerKey", source+"Waveforms")

    digiAlg = CompFactory.ScintWaveformDigiAlg(name, **kwargs)
    digiAlg.CB_alpha = dict_CB_param[source]["CB_alpha"]
    digiAlg.CB_n = dict_CB_param[source]["CB_n"]
    digiAlg.CB_mean = dict_CB_param[source]["CB_mean"]
    digiAlg.CB_sigma = dict_CB_param[source]["CB_sigma"]
    digiAlg.CB_norm = dict_CB_param[source]["CB_norm"]    

    digiAlg.base_mean = 15000
    digiAlg.base_rms = 3
     
    kwargs.setdefault("WaveformDigitisationTool", tool)

    acc.addEventAlgo(digiAlg)

    return acc

def ScintWaveformDigitizationOutputCfg(flags, **kwargs):
    """ Return ComponentAccumulator with output for Waveform Digi"""
    acc = ComponentAccumulator()
    ItemList = [
        "RawWaveformContainer#*"
    ]
    acc.merge(OutputStreamCfg(flags, "RDO"))
    ostream = acc.getEventAlgo("OutputStreamRDO")
    ostream.TakeItemsFromInput = True # Copies all data from input file to output
    ostream.ItemList += ItemList
    return acc
