#ifndef SCINTDIGIALGS_SCINTWAVEFORMDIGIALG_H
#define SCINTDIGIALGS_SCINTWAVEFORMDIGIALG_H

// Base class
#include "AthenaBaseComps/AthReentrantAlgorithm.h"

// Data classes
#include "WaveRawEvent/RawWaveformContainer.h"
#include "ScintSimEvent/ScintHitCollection.h"

// Tool classes
#include "WaveDigiTools/IWaveformDigitisationTool.h"

// Handles
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"

// Gaudi
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"

// ROOT
#include "TF1.h"


// STL
#include <string>

class ScintWaveformDigiAlg : public AthReentrantAlgorithm {

 public:
  // Constructor
  ScintWaveformDigiAlg(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~ScintWaveformDigiAlg() = default;

  /** @name Usual algorithm methods */
  //@{
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext& ctx) const override;
  virtual StatusCode finalize() override;
  //@}


 private:

  /** @name Disallow default instantiation, copy, assignment */
  //@{
  ScintWaveformDigiAlg() = delete;
  ScintWaveformDigiAlg(const ScintWaveformDigiAlg&) = delete;
  ScintWaveformDigiAlg &operator=(const ScintWaveformDigiAlg&) = delete;
  //@}

  Gaudi::Property<double> m_CB_alpha {this, "CB_alpha", 0, "Alpha of the crystal ball function"};
  Gaudi::Property<double> m_CB_n {this, "CB_n", 0, "n of the crystal ball function"};
  Gaudi::Property<double> m_CB_mean {this, "CB_mean", 0, "Mean of the crystal ball function"};  
  Gaudi::Property<double> m_CB_sigma {this, "CB_sigma", 0, "Sigma of the crystal ball function"};
  Gaudi::Property<double> m_CB_norm {this, "CB_norm", 0, "Norm of the crystal ball function"};

  Gaudi::Property<double> m_base_mean {this, "base_mean", 0, "Mean of the baseline"};
  Gaudi::Property<double> m_base_rms {this, "base_rms", 0, "RMS of the baseline"};

  /// Kernel PDF
  TF1* m_kernel;


  /**
   * @name Digitisation tool
   */
  ToolHandle<IWaveformDigitisationTool> m_digiTool
    {this, "WaveformDigitisationTool", "WaveformDigitisationTool"};


  /**
   * @name Input HITS using SG::ReadHandleKey
   */
  //@{

  SG::ReadHandleKey<ScintHitCollection> m_scintHitContainerKey 
  {this, "ScintHitContainerKey", ""};

  //@}


  /**
   * @name Output data using SG::WriteHandleKey
   */
  //@{
  SG::WriteHandleKey<RawWaveformContainer> m_waveformContainerKey
    {this, "WaveformContainerKey", ""};
  //@}

};

#endif // SCINTDIGIALGS_SCINTDIGIALG_H
