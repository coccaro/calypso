
#include "ScintWaveformDigiAlg.h"

#include "Identifier/Identifier.h"

#include <vector>
#include <map>
#include <utility>


ScintWaveformDigiAlg::ScintWaveformDigiAlg(const std::string& name, 
					 ISvcLocator* pSvcLocator)
  : AthReentrantAlgorithm(name, pSvcLocator) { 

}

StatusCode 
ScintWaveformDigiAlg::initialize() {
  ATH_MSG_INFO(name() << "::initalize()" );

  // Initalize tools
  ATH_CHECK( m_digiTool.retrieve() );


  // Set key to read waveform from
  ATH_CHECK( m_scintHitContainerKey.initialize() );

  // Set key to write container
  ATH_CHECK( m_waveformContainerKey.initialize() );

  // Will eventually depend on the type of detector
  // TODO: Vary time at which centre it?
  // TODO: Better parameters
 

  
  m_kernel = new TF1("PDF", "[4] * ROOT::Math::crystalball_pdf(x, [0],[1],[2],[3])", 0, 1200);
  m_kernel->SetParameter(0, m_CB_alpha);
  m_kernel->SetParameter(1, m_CB_n);
  m_kernel->SetParameter(2, m_CB_sigma);
  m_kernel->SetParameter(3, m_CB_mean);
  m_kernel->SetParameter(4, m_CB_norm);
  return StatusCode::SUCCESS;
}

StatusCode 
ScintWaveformDigiAlg::finalize() {
  ATH_MSG_INFO(name() << "::finalize()");

  delete m_kernel;

  return StatusCode::SUCCESS;
}

StatusCode 
ScintWaveformDigiAlg::execute(const EventContext& ctx) const {
  ATH_MSG_DEBUG("Executing");

  ATH_MSG_DEBUG("Run: " << ctx.eventID().run_number() 
		<< " Event: " << ctx.eventID().event_number());

  // Find the input HIT collection
  SG::ReadHandle<ScintHitCollection> scintHitHandle(m_scintHitContainerKey, ctx);

  ATH_CHECK( scintHitHandle.isValid() );
  ATH_MSG_DEBUG("Found ReadHandle for ScintHitCollection " << m_scintHitContainerKey);

  // Find the output waveform container
  SG::WriteHandle<RawWaveformContainer> waveformContainerHandle(m_waveformContainerKey, ctx);
  ATH_CHECK( waveformContainerHandle.record( std::make_unique<RawWaveformContainer>()) );

  ATH_MSG_DEBUG("WaveformsContainer '" << waveformContainerHandle.name() << "' initialized");

  if (scintHitHandle->size() == 0) {
    ATH_MSG_DEBUG("ScintHitCollection found with zero length!");
    return StatusCode::SUCCESS;
  }

  // Digitise the hits
  CHECK( m_digiTool->digitise<ScintHitCollection>(scintHitHandle.ptr(),
						  waveformContainerHandle.ptr(), m_kernel,
						  std::pair<float, float>(m_base_mean, m_base_rms)) );

  ATH_MSG_DEBUG("WaveformsHitContainer " << waveformContainerHandle.name() << "' filled with "<< waveformContainerHandle->size() <<" items");

  return StatusCode::SUCCESS;
}
