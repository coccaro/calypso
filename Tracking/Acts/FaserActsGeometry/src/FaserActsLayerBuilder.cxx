/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/
// ATHENA
#include "TrackerReadoutGeometry/SiDetectorElement.h"
#include "TrackerReadoutGeometry/SiDetectorElementCollection.h"
#include "TrackerReadoutGeometry/SCT_DetectorManager.h"

// PACKAGE
#include "FaserActsGeometry/FaserActsLayerBuilder.h"
#include "FaserActsGeometry/FaserActsDetectorElement.h"
#include "FaserActsGeometry/CuboidVolumeBuilder.h"
#include "ActsInterop/IdentityHelper.h"

// ACTS
#include "Acts/Material/ProtoSurfaceMaterial.hpp"
#include "Acts/Surfaces/CylinderSurface.hpp"
#include "Acts/Surfaces/PlaneSurface.hpp"
#include "Acts/Surfaces/RectangleBounds.hpp"
#include "Acts/Geometry/GenericApproachDescriptor.hpp"
#include "Acts/Geometry/ApproachDescriptor.hpp"
#include "Acts/Geometry/ProtoLayer.hpp"
#include "Acts/Geometry/LayerCreator.hpp"
#include "Acts/Geometry/GeometryContext.hpp"
#include "Acts/Definitions/Common.hpp"
#include "Acts/Definitions/Algebra.hpp"
#include "Acts/Definitions/Units.hpp"
#include "Acts/Utilities/BinningType.hpp"

using Acts::Surface;
using Acts::Transform3;
using Acts::Translation3;

using namespace Acts::UnitLiterals;

FaserActs::CuboidVolumeBuilder::Config FaserActsLayerBuilder::buildVolume(const Acts::GeometryContext& gctx)
{
  //Acts::CuboidVolumeBuilder::Config cvbConfig;
  FaserActs::CuboidVolumeBuilder::Config cvbConfig;
  //std::vector<Acts::CuboidVolumeBuilder::VolumeConfig> volumeConfigs = {};
  std::vector<FaserActs::CuboidVolumeBuilder::VolumeConfig> volumeConfigs = {};
  

  for (int iStation=1; iStation<4; iStation++) {

      //Acts::CuboidVolumeBuilder::VolumeConfig volumeConfig;
      FaserActs::CuboidVolumeBuilder::VolumeConfig volumeConfig;

      //std::vector<Acts::CuboidVolumeBuilder::LayerConfig> layerConfigs;
      std::vector<FaserActs::CuboidVolumeBuilder::LayerConfig> layerConfigs;
      layerConfigs.clear();

      for (int iPlane=0; iPlane<3; iPlane++) {

         m_cfg.station = iStation;
         m_cfg.plane = iPlane;
         std::vector<std::shared_ptr<const Acts::Surface>> surfaces;
         surfaces.clear();


        //Acts::CuboidVolumeBuilder::SurfaceConfig surfacecfg;
        FaserActs::CuboidVolumeBuilder::SurfaceConfig surfacecfg;
    
        //Acts::CuboidVolumeBuilder::LayerConfig layercfg;
        FaserActs::CuboidVolumeBuilder::LayerConfig layercfg;
        layercfg.binsX = 2;
        layercfg.binsY = 4;

        buildLayers(gctx, surfaces, layercfg, surfacecfg);

	layercfg.surfaceCfg = surfacecfg;
        layercfg.active = true;

        layercfg.surfaces = surfaces;
        layerConfigs.push_back(layercfg);

        if (iPlane == 1) {
          volumeConfig.position = Acts::Vector3(0, 0, surfacecfg.position.z()); 
        }
        if (iStation == 0 && iPlane == 1) { 
	  cvbConfig.position  = Acts::Vector3(0, 0, surfacecfg.position.z());
        }
    }

      volumeConfig.length   = m_trackerDimensions;
      volumeConfig.layerCfg = layerConfigs;
      volumeConfig.name     = "Station_" + std::to_string(iStation);
      volumeConfigs.push_back(volumeConfig);
  }
    
  //cvbConfig.position  = m_worldCenter;
  cvbConfig.length    = m_worldDimensions;
  cvbConfig.volumeCfg = volumeConfigs;

  return cvbConfig;
}

void
FaserActsLayerBuilder::buildLayers(const Acts::GeometryContext& gctx,
    std::vector<std::shared_ptr<const Surface>>& surfaces,
    FaserActs::CuboidVolumeBuilder::LayerConfig& layercfg,
    FaserActs::CuboidVolumeBuilder::SurfaceConfig& surfacecfg)
{

  auto siDetMng = static_cast<const TrackerDD::SCT_DetectorManager*>(m_cfg.mng);
  
  for (int iRow = 0; iRow < 4; iRow++) {
     for (int iModule = -1; iModule < 2; iModule++) {
        for (int iSensor = 0; iSensor < 2; iSensor++) {
        //for (int iSensor = 0; iSensor < 1; iSensor++) {  // only use the first sensor to construct the surface

	   if (iModule == 0) continue;
	   const TrackerDD::SiDetectorElement* siDetElement = siDetMng->getDetectorElement(m_cfg.station, m_cfg.plane, iRow, iModule, iSensor) ;

           if (logger().doPrint(Acts::Logging::VERBOSE)) {
	     ACTS_VERBOSE("Found SCT sensor (" << m_cfg.station << "/" << m_cfg.plane << "/" << iRow << "/" << iModule << "/" << iSensor << ") with global center at (" << siDetElement->center().x() << ", " << siDetElement->center().y() << ", " << siDetElement->center().z() << ")." );
	   }

           auto element = std::make_shared<const FaserActsDetectorElement>(siDetElement);

           surfaces.push_back(element->surface().getSharedPtr());

           m_cfg.elementStore->push_back(element);
    
           m_ModuleWidth = siDetElement->width();
           m_ModuleLength = siDetElement->length();
       }
     }
  }


  Acts::ProtoLayer pl(gctx, surfaces);

  if (logger().doPrint(Acts::Logging::VERBOSE)) {
      ACTS_VERBOSE(" Plane's zMin / zMax: " << pl.min(Acts::binZ) << " / " << pl.max(Acts::binZ));
  }

  std::shared_ptr<const Acts::ProtoSurfaceMaterial> materialProxy = nullptr;

  double layerZ = 0.5 * (pl.min(Acts::binZ) + pl.max(Acts::binZ));
  double layerThickness = (pl.max(Acts::binZ) - pl.min(Acts::binZ));
  double layerZInner = layerZ - layerThickness/2.;
  double layerZOuter = layerZ + layerThickness/2.;

  surfacecfg.position = Acts::Vector3(0, 0, layerZ);

  if (std::abs(layerZInner) > std::abs(layerZOuter)) std::swap(layerZInner, layerZOuter);

      auto rBounds = std::make_shared<const Acts::RectangleBounds>( 0.5*layercfg.binsY*m_ModuleWidth, 0.5*layercfg.binsX*m_ModuleLength ) ;

      Transform3 transformNominal(Translation3(0., 0., layerZ));
      
      Transform3 transformInner(Translation3(0., 0., layerZInner));
      
      Transform3 transformOuter(Translation3(0., 0., layerZOuter));

      std::shared_ptr<Acts::PlaneSurface> innerBoundary 
        = Acts::Surface::makeShared<Acts::PlaneSurface>(transformInner, rBounds);
      
      std::shared_ptr<Acts::PlaneSurface> nominalSurface 
        = Acts::Surface::makeShared<Acts::PlaneSurface>(transformNominal, rBounds);
      
      std::shared_ptr<Acts::PlaneSurface> outerBoundary 
        = Acts::Surface::makeShared<Acts::PlaneSurface>(transformOuter, rBounds);

      size_t matBinsX = layercfg.binsX;
      size_t matBinsY = layercfg.binsY;

      Acts::BinUtility materialBinUtil(
          matBinsX, -0.5*layercfg.binsY*m_ModuleWidth, 0.5*layercfg.binsY*m_ModuleWidth, Acts::open, Acts::binX);
      materialBinUtil += Acts::BinUtility(
          matBinsY, -0.5*layercfg.binsX*m_ModuleLength, 0.5*layercfg.binsX*m_ModuleLength, Acts::open, Acts::binY, transformInner);
      
      materialProxy
        = std::make_shared<const Acts::ProtoSurfaceMaterial>(materialBinUtil);

      ACTS_VERBOSE("[L] Layer is marked to carry support material on Surface ( "
          "inner=0 / center=1 / outer=2 ) : " << "inner");
      ACTS_VERBOSE("with binning: [" << matBinsX << ", " << matBinsY << "]");

      ACTS_VERBOSE("Created ApproachSurfaces for layer at:");
      ACTS_VERBOSE(" - inner:   Z=" << layerZInner);
      ACTS_VERBOSE(" - central: Z=" << layerZ);
      ACTS_VERBOSE(" - outer:   Z=" << layerZOuter);
      

      // set material on inner
      // @TODO: make this configurable somehow
      innerBoundary->assignSurfaceMaterial(materialProxy);

      std::vector<std::shared_ptr<const Acts::Surface>> aSurfaces;
      aSurfaces.push_back(std::move(innerBoundary));
      aSurfaces.push_back(std::move(nominalSurface));
      aSurfaces.push_back(std::move(outerBoundary));

      layercfg.approachDescriptor = new Acts::GenericApproachDescriptor(aSurfaces);
      
}

