
# Declare the package name:
atlas_subdir( FaserActsGeometry )

# External dependencies:
find_package( CLHEP )
find_package( Eigen )
find_package( Boost )
find_package( nlohmann_json )

find_package( Acts COMPONENTS Core )

atlas_add_library( FaserActsGeometryLib 
			src/FaserActsSurfaceMappingTool.cxx
			src/FaserActsMaterialMapping.cxx
		    src/FaserActsAlignmentStore.cxx
		    src/FaserActsDetectorElement.cxx
		    src/FaserActsLayerBuilder.cxx
		    src/CuboidVolumeBuilder.cxx
#			src/FaserActsJsonGeometryConverter.cxx
		    src/util/*.cxx
		    PUBLIC_HEADERS FaserActsGeometry
		    INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${BOOST_INCLUDE_DIRS}
		    LINK_LIBRARIES ${CLHEP_LIBRARIES} ${EIGEN_LIBRARIES} ${BOOST_LIBRARIES} nlohmann_json::nlohmann_json
	       	    TrackerIdentifier 
		   		TrackerReadoutGeometry
		   		ActsInteropLib
				FaserActsGeometryInterfacesLib
		   		AthenaKernel												                         
		   		ActsCore
		   		GeoModelFaserUtilities
			 	ActsGeometryLib
			 	ActsGeometryInterfacesLib
		   		MagFieldInterfaces 
		   		MagFieldElements 
		   		MagFieldConditions
		   		TrackerRawData 
		   		TrackerSimData 
		   		GeneratorObjects 
		   		TrackerSimEvent 
		   		TrackerSpacePoint 
		   		TrackerIdentifier 
		   		TrackerPrepRawData
		   		TrkSpacePoint
			 	TrkGeometry
		  )


# Component(s) in the package:
atlas_add_component( FaserActsGeometry
##src/*.cxx
			    src/FaserActsSurfaceMappingTool.cxx
				src/FaserActsMaterialMapping.cxx
#				src/FaserActsMaterialJsonWriterTool.cxx
#				src/FaserActsJsonGeometryConverter.cxx
		        src/FaserActsTrackingGeometrySvc.cxx
		        src/FaserActsTrackingGeometryTool.cxx
                src/FaserActsWriteTrackingGeometry.cxx
		        src/FaserActsObjWriterTool.cxx
		        src/FaserActsExtrapolationAlg.cxx
		        src/FaserActsExtrapolationTool.cxx
		        src/FaserActsPropStepRootWriterSvc.cxx
		        src/FaserActsAlignmentCondAlg.cxx
		        src/NominalAlignmentCondAlg.cxx
#src/FaserActsKalmanFilterAlg.cxx
                     src/FaserActsVolumeMappingTool.cxx
		     src/components/*.cxx
                     PUBLIC_HEADERS FaserActsGeometry
                     INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${BOOST_INCLUDE_DIRS}
                     LINK_LIBRARIES ${CLHEP_LIBRARIES} ${EIGEN_LIBRARIES} ${BOOST_LIBRARIES}
		     EventInfo
		     AthenaBaseComps
		     GaudiKernel
		     FaserActsGeometryLib
		     ActsInteropLib
		     FaserActsGeometryInterfacesLib
             ActsCore
			 ActsGeometryLib
			 ActsGeometryInterfacesLib
			 GeoModelFaserUtilities
             MagFieldInterfaces
             MagFieldElements
             MagFieldConditions
		     TrackerRawData 
		     TrackerSimData 
		     GeneratorObjects 
	  	     TrackerSimEvent 
	   	     TrackerSpacePoint 
	   	     TrackerIdentifier 
	   	     TrackerPrepRawData
	   	     TrkSpacePoint
			 TrkGeometry

	 	   )

# Install files from the package:
atlas_install_headers( FaserActsGeometry )
atlas_install_python_modules( python/*.py )
atlas_install_scripts( test/*.py )

