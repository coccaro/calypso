// Copyright (C) 2019 CERN for the benefit of the Acts project
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/

#pragma once

#include <unordered_map>
#include <utility>

// ACTS
#include "Acts/EventData/MultiTrajectory.hpp"
#include "Acts/EventData/TrackParameters.hpp"

// PACKAGE
#include "FaserActsKalmanFilter/IndexSourceLink.h"

using IndexedParams = std::unordered_map<size_t, Acts::BoundTrackParameters>;

struct FaserActsRecMultiTrajectory
{
 public:
  FaserActsRecMultiTrajectory() = default;

  FaserActsRecMultiTrajectory(const Acts::MultiTrajectory<IndexSourceLink>& multiTraj,
                     const std::vector<size_t>& tTips,
                     const IndexedParams& parameters)
      : m_multiTrajectory(multiTraj),
        m_trackTips(tTips),
        m_trackParameters(parameters) {}

  FaserActsRecMultiTrajectory(const FaserActsRecMultiTrajectory& rhs)
      : m_multiTrajectory(rhs.m_multiTrajectory),
        m_trackTips(rhs.m_trackTips),
        m_trackParameters(rhs.m_trackParameters) {}

  FaserActsRecMultiTrajectory(FaserActsRecMultiTrajectory&& rhs)
      : m_multiTrajectory(std::move(rhs.m_multiTrajectory)),
        m_trackTips(std::move(rhs.m_trackTips)),
        m_trackParameters(std::move(rhs.m_trackParameters)) {}

  ~FaserActsRecMultiTrajectory() = default;

  FaserActsRecMultiTrajectory& operator=(const FaserActsRecMultiTrajectory& rhs) {
    m_multiTrajectory = rhs.m_multiTrajectory;
    m_trackTips = rhs.m_trackTips;
    m_trackParameters = rhs.m_trackParameters;
    return *this;
  }

  FaserActsRecMultiTrajectory& operator=(FaserActsRecMultiTrajectory&& rhs) {
    m_multiTrajectory = std::move(rhs.m_multiTrajectory);
    m_trackTips = std::move(rhs.m_trackTips);
    m_trackParameters = std::move(rhs.m_trackParameters);
    return *this;
  }

  bool hasTrajectory(const size_t& entryIndex) const {
    return std::count(m_trackTips.begin(), m_trackTips.end(), entryIndex) > 0;
  }

  bool hasTrackParameters(const size_t& entryIndex) const {
    return m_trackParameters.count(entryIndex) > 0;
  }

  std::pair<std::vector<size_t>, Acts::MultiTrajectory<IndexSourceLink>>
  trajectory() const {
    return std::make_pair(m_trackTips, m_multiTrajectory);
  }

  const Acts::BoundTrackParameters& trackParameters(const size_t& entryIndex) const {
    auto it = m_trackParameters.find(entryIndex);
    if (it != m_trackParameters.end()) {
      return it->second;
    } else {
      throw std::runtime_error(
          "No fitted track parameters for trajectory with entry index = " +
          std::to_string(entryIndex));
    }
  }

 private:
  Acts::MultiTrajectory<IndexSourceLink> m_multiTrajectory;
  std::vector<size_t> m_trackTips = {};
  IndexedParams m_trackParameters = {};
};
