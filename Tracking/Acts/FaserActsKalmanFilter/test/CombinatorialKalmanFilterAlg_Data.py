#!/usr/bin/env python

import sys
from AthenaCommon.Logging import log, logging
from AthenaCommon.Constants import DEBUG, VERBOSE, INFO
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
from WaveRecAlgs.WaveRecAlgsConfig import WaveformReconstructionCfg
from TrackerPrepRawDataFormation.TrackerPrepRawDataFormationConfig import FaserSCT_ClusterizationCfg
from TrackerSpacePointFormation.TrackerSpacePointFormationConfig import TrackerSpacePointFinderCfg
from TrackerSeedFinder.TrackerSeedFinderConfig import TrackerSeedFinderCfg
from FaserActsKalmanFilter.CombinatorialKalmanFilterConfig import CombinatorialKalmanFilterCfg

log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

ConfigFlags.Input.Files = ['/eos/project-f/faser-commissioning/TI12Data/Run-004411/Faser-Physics-004411-00000.raw']
ConfigFlags.Output.ESDFileName = "myCKF.ESD.pool.root"
ConfigFlags.Output.AODFileName = "myCKF.AOD.pool.root"
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"
ConfigFlags.IOVDb.DatabaseInstance = "OFLP200"               # Use MC conditions for now
ConfigFlags.Input.ProjectName = "data21"                     # Needed to bypass autoconfig
ConfigFlags.Input.isMC = False                               # Needed to bypass autoconfig
ConfigFlags.GeoModel.FaserVersion     = "FASER-01"           # FASER geometry
ConfigFlags.Common.isOnline = False
ConfigFlags.GeoModel.Align.Dynamic = False
ConfigFlags.Beam.NumberOfCollisions = 0.
ConfigFlags.Detector.GeometryFaserSCT = True
ConfigFlags.lock()

acc = MainServicesCfg(ConfigFlags)
#acc.merge(PoolReadCfg(ConfigFlags))
acc.merge(PoolWriteCfg(ConfigFlags))
from FaserByteStreamCnvSvc.FaserByteStreamCnvSvcConfig import FaserByteStreamCnvSvcCfg               
acc.merge(FaserByteStreamCnvSvcCfg(ConfigFlags))
acc.merge(WaveformReconstructionCfg(ConfigFlags))
acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags, DataObjectName="SCT_EDGEMODE_RDOs"))
acc.merge(TrackerSpacePointFinderCfg(ConfigFlags))
acc.merge(TrackerSeedFinderCfg(ConfigFlags))
acc.merge(CombinatorialKalmanFilterCfg(ConfigFlags))

logging.getLogger('forcomps').setLevel(VERBOSE)
acc.foreach_component("*").OutputLevel = VERBOSE
acc.foreach_component("*ClassID*").OutputLevel = INFO
acc.getService("StoreGateSvc").Dump = True
acc.getService("ConditionStore").Dump = True
acc.printConfig(withDetails=True)
ConfigFlags.dump()

sc = acc.run(maxEvents=-1)

sys.exit(not sc.isSuccess())
