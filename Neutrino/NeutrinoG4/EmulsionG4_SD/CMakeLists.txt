################################################################################
# Package: EmulsionG4_SD
################################################################################

# Declare the package name:
atlas_subdir( EmulsionG4_SD )

# External dependencies:
find_package( CLHEP )
find_package( Geant4 )
find_package( XercesC )

# Component(s) in the package:
atlas_add_component( EmulsionG4_SD
                     src/*.cxx
                     src/components/*.cxx
                     INCLUDE_DIRS ${GEANT4_INCLUDE_DIRS} ${XERCESC_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
                     LINK_LIBRARIES ${GEANT4_LIBRARIES} ${XERCESC_LIBRARIES} ${CLHEP_LIBRARIES} StoreGateLib SGtests GaudiKernel NeutrinoSimEvent G4AtlasToolsLib FaserMCTruth )

# Install files from the package:
atlas_install_python_modules( python/*.py )

